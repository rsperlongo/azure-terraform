variable "name-rg" {
  type = string
  description = "Nome do Resource Group"
  default = "rg-variaveis"
}

variable "location" {
  type = string
  description = "Localização dos Recursos do Azure. Ex: brazilsouth "
  default = "eastus"
}

variable "tags" {
  type = map
  description = "Tagas nos Recursos e Servicos do Azure"
  default = {
    ambiente = "desenvolvimento"
    responsavel = "Ricardo Sperlongo"
  }
}

variable "vnetenderecos" {
  type = list
  default = ["10.0.0.0/16", "192.168.0.0/16"]
}